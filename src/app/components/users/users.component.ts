import { Component, OnInit } from '@angular/core';
import { UserService } from './../../core/services/UserService';
import { UserDto } from './../../core/dto/UserDto';
import {Router} from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  userList: UserDto[] = [];
  constructor(private userService: UserService, private router:Router) { }


  ngOnInit(): void {
    try {
      this.userService.getAllUsers().subscribe(data => {
        this.userList = data;
      },
        error => {
        }
      );

    } catch (error) {
      console.log(error);
    }
  }

  updateUser(userId){
    console.log(userId);
    this.router.navigate(['users/update', userId]);
  }

}
