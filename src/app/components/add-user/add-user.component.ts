import { Component, OnInit } from '@angular/core';
import { DocumentTypeService } from './../../core/services/DocumentTypeService';
import { DocumentTypeDto } from './../../core/dto/DocumentTypeDto';
import { UserDto } from 'src/app/core/dto/UserDto';
import { NgForm } from '@angular/forms';
import { UserService } from 'src/app/core/services/UserService';
import { Constants } from 'src/app/core/constants/Constants';


@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  docTypeList: DocumentTypeDto[] = [];
  user: UserDto;
  showResponseMessage:boolean;
  responseMessage:string;
  responseType:string;

  constructor(private documentTypeService: DocumentTypeService, private userService: UserService) { }

  ngOnInit(): void {
    try {
      this.showResponseMessage = false;
      this.responseMessage = "";
      this.responseType = Constants.WARNING_STATUS_CSS_CLASS;

      this.user = new UserDto();
      this.user.Id = 0;
      this.user.status = 'A';

      this.documentTypeService.getAllDocumentTypes ().subscribe(data => {
        this.docTypeList = data;
        this.user.documentTypeId = this.docTypeList[0].id;
      },
        error => {
          console.log("Error al cargar datos del servidor.");
        }
      );
    } catch (error) {
      console.log(error);
    }
  }

  addUser(form:NgForm){
    this.showResponseMessage = false;
    if(form.valid){
      this.userService.createUser(this.user).subscribe(data => {
        this.processResponse(data);
      },
        error => {
          console.log("Error al intentar crear Usuario: " + JSON.stringify(error));
        }
      );
      
    }else{
      console.log("Formulario de Usuario tiene errores.");
      return;
    }
  }

  processResponse(response:any){
    this.showResponseMessage = true;
    if(response){
      if(response.status){
        this.responseType = Constants.SUCCESS_STATUS_CSS_CLASS;
      }else if(response.statusCode == Constants.API_RESPONSE_STATUS_CODE_ERROR){
        this.responseType = Constants.ERROR_STATUS_CSS_CLASS;
      }else{
        this.responseType = Constants.WARNING_STATUS_CSS_CLASS;
      }

      this.responseMessage = response.message;
      
    }else{
      this.responseType = Constants.WARNING_STATUS_CSS_CLASS;
      this.responseMessage = "No se pudo obtener una respuesta del Servidor";
    }


  }

}
