import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DocumentTypeService } from './../../core/services/DocumentTypeService';
import { DocumentTypeDto } from './../../core/dto/DocumentTypeDto';
import { UserDto } from 'src/app/core/dto/UserDto';
import { NgForm } from '@angular/forms';
import { UserService } from 'src/app/core/services/UserService';
import { Constants } from 'src/app/core/constants/Constants';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  userId: string;
  docTypeList: DocumentTypeDto[] = [];
  user: UserDto;
  showResponseMessage: boolean;
  responseMessage: string;
  responseType: string;

  constructor(private documentTypeService: DocumentTypeService, private userService: UserService, private activatedRoute: ActivatedRoute) {

    this.activatedRoute.params.subscribe(params => {
      this.userId = params['userId'];
    });

  }

  ngOnInit(): void {
    try {
      this.showResponseMessage = false;
      this.responseMessage = "";
      this.responseType = Constants.WARNING_STATUS_CSS_CLASS;
      this.user = new UserDto();

      /*Se cargan el usuario medinate elkId*/
      this.userService.getUserById(this.userId).subscribe(data => {
        if (data) {
          this.user = data;
        } else {
          this.user = null;
          this.showResponseMessage = true;
          this.responseMessage = "Usuario no encontrado!";
          this.responseType = Constants.ERROR_STATUS_CSS_CLASS;
        }
      },
        error => {
          console.log("Error al cargar datos del Usuario.");
        }
      );


      /*Se cargan los tipo de documento*/
      this.documentTypeService.getAllDocumentTypes().subscribe(data => {
        this.docTypeList = data;
      },
        error => {
          console.log("Error al cargar datos del servidor.");
        }
      );
    } catch (error) {
      console.log(error);
    }
  }

  updateUser(form: NgForm) {
    this.showResponseMessage = false;
    console.log(this.user);
    if (form.valid) {
      this.userService.updateUser(this.user).subscribe(data => {
        this.processResponse(data);
      },
        error => {
          console.log("Error al actualizar  Usuario: " + JSON.stringify(error));
        }
      );

    } else {
      console.log("Formulario de Usuario tiene errores.");
      return;
    }
  }

  processResponse(response: any) {
    this.showResponseMessage = true;
    if (response) {
      if (response.status) {
        this.responseType = Constants.SUCCESS_STATUS_CSS_CLASS;
      } else if (response.statusCode == Constants.API_RESPONSE_STATUS_CODE_ERROR) {
        this.responseType = Constants.ERROR_STATUS_CSS_CLASS;
      } else {
        this.responseType = Constants.WARNING_STATUS_CSS_CLASS;
      }

      this.responseMessage = response.message;

    } else {
      this.responseType = Constants.WARNING_STATUS_CSS_CLASS;
      this.responseMessage = "No se pudo obtener una respuesta del Servidor";
    }


  }

}
