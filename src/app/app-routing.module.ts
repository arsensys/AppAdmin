import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {UsersComponent} from './components/users/users.component';
import {AddUserComponent} from './components/add-user/add-user.component';
import {UpdateUserComponent} from './components/update-user/update-user.component';

const routes: Routes = [ 
  {path:'home', component:HomeComponent},
  {path:'users', component:UsersComponent},
  {path:'users/add', component:AddUserComponent},
  {path:'users/update/:userId', component:UpdateUserComponent},
  {path:'**', pathMatch:'full', component:HomeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
