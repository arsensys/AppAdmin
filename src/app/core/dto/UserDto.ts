export class UserDto{
    Id:number;
	documentTypeId:number
	document:string;
	names:string;
	lastNames:string;
	email:string;
	phone:string;
	status:string;
}