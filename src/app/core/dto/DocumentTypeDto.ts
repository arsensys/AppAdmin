export class DocumentTypeDto{
    id:number;
	abbreviation:string;
	description:string;
}