import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import {DocumentTypeDto} from '../dto/DocumentTypeDto';

@Injectable({
  providedIn: 'root'
})
export class DocumentTypeService {

  documentTypes:DocumentTypeDto[];
  serverApiUrl = environment.apiUrl;
  
  constructor(public http: HttpClient) { }


  public getAllDocumentTypes() {
    return this.http.get<DocumentTypeDto[]>(this.serverApiUrl+'api/documentTypes/list')         
  }

}
