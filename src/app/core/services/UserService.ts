import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import {UserDto} from './../../core/dto/UserDto';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  users:UserDto[];
  serverApiUrl = environment.apiUrl;
  
  constructor(public http: HttpClient) { }


  public getAllUsers() {
    return this.http.get<UserDto[]>(this.serverApiUrl+'api/users/list')         
  }

  public getUserById(userId:string) {

    let params = new HttpParams().set('userId', userId);
    return this.http.get<UserDto>(this.serverApiUrl+'api/users/getUser', {params:params});  
  }

  public createUser(user:UserDto) {
    return this.http.post(this.serverApiUrl+'api/users/create', user);
  }

  public updateUser(user:UserDto) {
    return this.http.post(this.serverApiUrl+'api/users/update', user);
  }

}
