export class Constants{
    static readonly SUCCESS_STATUS_CSS_CLASS = "success";
	static readonly ERROR_STATUS_CSS_CLASS = "danger";
	static readonly WARNING_STATUS_CSS_CLASS = "warning";
	static readonly INFO_STATUS_CSS_CLASS = "info";

    static readonly  API_RESPONSE_STATUS_CODE_SUCCESS = "1";
	static readonly  API_RESPONSE_STATUS_CODE_ERROR = "0";
	static readonly  API_RESPONSE_STATUS_CODE_WARNING = "2";
	static readonly  API_RESPONSE_STATUS_CODE_INFO = "3";
}